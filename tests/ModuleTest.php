<?php

namespace ServiceCore\View\Test;

use Laminas\EventManager\EventManager;
use Laminas\EventManager\SharedEventManager;
use Laminas\Mvc\Application;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\View\Module;
use ServiceCore\View\RenderStrategy\Pdf as PdfRenderStrategy;

class ModuleTest extends TestCase
{
    public function testGetConfig(): void
    {
        $module = new Module();

        self::assertNotEmpty($module->getConfig());
    }

    public function testOnBootstrap(): void
    {
        $module  = new Module();
        $builder = $this->getMockBuilder(MvcEvent::class);

        $builder->disableOriginalConstructor();
        $builder->onlyMethods(
            [
                'getApplication'
            ]
        );

        $event   = $builder->getMock();
        $builder = $this->getMockBuilder(Application::class);

        $builder->disableOriginalConstructor();
        $builder->onlyMethods(
            [
                'getServiceManager',
                'getEventManager'
            ]
        );

        $application = $builder->getMock();
        $builder     = $this->getMockBuilder(ServiceManager::class);

        $builder->disableOriginalConstructor();
        $builder->onlyMethods(
            [
                'get'
            ]
        );

        $serviceManager = $builder->getMock();
        $builder        = $this->getMockBuilder(PdfRenderStrategy::class);

        $builder->disableOriginalConstructor();

        $pdfStrategy = $builder->getMock();

        $serviceManager->expects(self::once())
                       ->method('get')
                       ->willReturn($pdfStrategy);

        $application->expects(self::once())
                    ->method('getServiceManager')
                    ->willReturn($serviceManager);

        $builder = $this->getMockBuilder(EventManager::class);

        $builder->disableOriginalConstructor();
        $builder->onlyMethods(
            [
                'getSharedManager'
            ]
        );

        $eventManager  = $builder->getMock();
        $builder       = $this->getMockBuilder(SharedEventManager::class);
        $sharedManager = $builder->getMock();

        $eventManager->expects(self::once())
                     ->method('getSharedManager')
                     ->willReturn($sharedManager);

        $application->expects(self::once())
                    ->method('getEventManager')
                    ->willReturn($eventManager);

        $event->expects(self::exactly(2))
              ->method('getApplication')
              ->willReturn($application);

        $module->onBootstrap($event);
    }
}
