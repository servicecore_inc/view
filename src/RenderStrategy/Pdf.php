<?php

namespace ServiceCore\View\RenderStrategy;

use Laminas\EventManager\SharedEventManagerInterface;
use Laminas\Http\Header\CacheControl;
use Laminas\Http\Header\ContentDisposition;
use Laminas\Http\Header\ContentLength;
use Laminas\Http\Header\ContentType;
use Laminas\Http\Header\Expires;
use Laminas\Http\Header\Pragma;
use Laminas\Http\Headers;
use Laminas\Http\PhpEnvironment\Response;
use Laminas\Http\Response as ZendResponse;
use Laminas\Http\Response\Stream;
use Laminas\Mvc\MvcEvent;
use SplFileInfo;
use ServiceCore\View\Renderer\Pdf as PdfRenderer;
use ServiceCore\View\ViewModel\Pdf as PdfViewModel;

class Pdf
{
    /** @var PdfRenderer */
    private $renderer;

    public function __construct(PdfRenderer $renderer)
    {
        $this->renderer = $renderer;
    }

    public function attachShared(SharedEventManagerInterface $events, int $priority = 100): void
    {
        $events->attach('*', MvcEvent::EVENT_RENDER, [$this, 'injectResponse'], $priority);
    }

    public function injectResponse(MvcEvent $e): ?ZendResponse
    {
        $result = $e->getResult();
        if ($result instanceof Response) {
            return $result;
        }

        if (!$result instanceof PdfViewModel) {
            return null;
        }

        $file = $this->renderer->render($result);
        if (!$file instanceof SplFileInfo) {
            return null;
        }

        $response = new Stream();

        $response->setStream(\fopen($file->getRealPath(), 'rb'));
        $response->setStreamName(\basename($file->getBasename()));
        $response->setHeaders($this->buildHeaders($file, $result));
        $response->setStatusCode(Response::STATUS_CODE_200);

        $e->setResponse($response);
        $e->stopPropagation(true);

        return $response;
    }

    private function buildHeaders(SplFileInfo $splFileInfo, PdfViewModel $model): Headers
    {
        if ($model->getFilename()) {
            $fileName = $model->getFilename() . '.pdf';
        } else {
            $fileName = $splFileInfo->getBasename();
        }

        $headers = new Headers();

        $headers->addHeader(new ContentDisposition(\sprintf('attachment; filename="%s"', $fileName)))
                ->addHeader(new ContentType('application/octet-stream'))
                ->addHeader(new ContentLength($splFileInfo->getSize()))
                ->addHeader(new Expires())
                ->addHeader(new CacheControl())
                ->addHeader(new Pragma('public'));

        return $headers;
    }
}
