<?php

namespace ServiceCore\View\RenderStrategy\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\View\Renderer\Pdf as PdfRenderer;
use ServiceCore\View\RenderStrategy\Pdf as PdfStrategy;

class Pdf implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return PdfStrategy
     */
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): PdfStrategy {
        /** @var PdfRenderer $renderer */
        $renderer = $container->get(PdfRenderer::class);

        return new PdfStrategy($renderer);
    }
}
