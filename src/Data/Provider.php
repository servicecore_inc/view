<?php

namespace ServiceCore\View\Data;

interface Provider
{
    public function getPdfFilename(): string;

    public function getPdfTemplate(): string;

    public function getPdfTemplateFooter(): ?string;
}
