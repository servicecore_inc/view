<?php

namespace ServiceCore\View\Renderer\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\View\Renderer\PhpRenderer;
use ServiceCore\View\Config\Create as CreateConfig;
use ServiceCore\View\Renderer\Pdf as PdfRenderer;

class Pdf implements FactoryInterface
{
    /**
     * @param ContainerInterface $container
     * @param string             $requestedName
     * @param array|null         $options
     *
     * @return PdfRenderer
     */
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): PdfRenderer {
        /** @var PhpRenderer $pdfRenderer */
        $phpRenderer = $container->get(PhpRenderer::class);

        /** @var CreateConfig $config */
        $config = $container->get(CreateConfig::class);

        return new PdfRenderer($phpRenderer, $config);
    }
}
