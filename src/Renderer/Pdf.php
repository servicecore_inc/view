<?php

namespace ServiceCore\View\Renderer;

use Knp\Snappy\Pdf as SnappyPdf;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Renderer\RendererInterface as Renderer;
use Laminas\View\Resolver\ResolverInterface;
use ServiceCore\View\Config\Create as CreateConfig;
use SplFileInfo;
use ServiceCore\View\ViewModel\Pdf as PdfViewModel;

class Pdf implements Renderer
{
    /** @var PhpRenderer */
    private $renderer;

    /** @var CreateConfig */
    private $config;

    public function __construct(PhpRenderer $renderer, CreateConfig $config)
    {
        $this->renderer = $renderer;
        $this->config   = $config;
    }

    public function getEngine()
    {
        return null;
    }

    public function setResolver(ResolverInterface $resolver): self
    {
        return $this;
    }

    /**
     * Processes a view script and returns the output.
     *
     * @param string|PdfViewModel $nameOrModel The script/resource process, or a view model
     * @param array|null          $values Values to use during rendering
     *
     * @return SplFileInfo the file info for the generated PDF
     */
    public function render($nameOrModel, $values = null): SplFileInfo
    {
        $html = $this->renderer->render($nameOrModel, $values);
        $path = $this->config->getPath() . \str_replace('.', '', \uniqid('pdf_', true)) . '.pdf';
        $file = new SnappyPdf(
            $this->config->getBinary(),
            $this->config->getPdfOptions()
        );

        if (\array_key_exists('landscape', $nameOrModel->params ?? []) && $nameOrModel->params['landscape']) {
            $file->setOption('orientation', 'Landscape');
        }

        $file->generateFromHtml($html, $path);

        return new SplFileInfo($path);
    }
}
