<?php

namespace ServiceCore\View\Config\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Exception\ServiceNotCreatedException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\View\Config\Create as CreateConfig;

class Create implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null): CreateConfig
    {
        /** @var array $configArray */
        $configArray = $container->get('config');

        if (!\array_key_exists('pdf', $configArray)) {
            throw new ServiceNotCreatedException('Must specify a `pdf` key in root config array');
        }

        $configArray = $configArray['pdf'];

        return new CreateConfig($configArray);
    }
}
