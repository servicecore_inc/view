<?php

namespace ServiceCore\View\Config;

use Laminas\Stdlib\AbstractOptions;

class Create extends AbstractOptions
{
    /**
     * Path to the wkhtmltopdf binary
     *
     * @var string
     */
    private $binary;

    /**
     * Path to the folder where files are stored
     *
     * @var string
     */
    private $path;

    /**
     * The map of routeName => PDF name and options
     * @var array
     */
    private $templateMap;

    /**
     * @var array
     */
    private $transformerMap;

    /**
     * Use low quality rendering
     *
     * @var bool
     */
    private $isLowQuality = false;

    /**
     * Page size to use when rendering the PDF
     *
     * Documentation -
     * @see http://madalgo.au.dk/~jakobt/wkhtmltoxdoc/wkhtmltopdf_0.10.0_rc2-doc.html#Page%20sizes
     *
     * Accepted values (think Letter, A4, Legal, etc) -
     * @see http://doc.qt.io/archives/qt-4.8/qprinter.html#PaperSize-enum
     *
     * @todo Validate options passed to setPageSize based on list above
     * @todo Allow --page-width and --page-height options as well
     *
     * @var string
     */
    private $pageSize = 'Letter';

    public function __construct($options = null)
    {
        $options = $this->checkDefaults($options);

        parent::__construct($options);
    }

    public function getBinary(): ?string
    {
        return $this->binary;
    }

    public function setBinary(string $binary): self
    {
        $this->binary = $binary;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        if (\substr($path, -1) !== '/') {
            $path .= '/';
        }

        if (!\is_dir($path) || !\is_writable($path)) {
            throw new \InvalidArgumentException(
                \sprintf('Provided path does not exist or is not writable (%s)', $path)
            );
        }

        $this->path = $path;

        return $this;
    }

    public function getPdfOptions(): array
    {
        return [
            'lowquality'               => $this->isLowQuality(),
            'page-size'                => $this->getPageSize(),
            'enable-local-file-access' => true
        ];
    }

    public function isLowQuality(): bool
    {
        return $this->isLowQuality;
    }

    public function getTemplateMap(): array
    {
        return $this->templateMap;
    }

    public function setTemplateMap(array $templateMap): void
    {
        $this->templateMap = $templateMap;
    }

    public function getTransformerMap(): array
    {
        return $this->transformerMap;
    }

    public function setTransformerMap(array $transformerMap): self
    {
        $this->transformerMap = $transformerMap;

        return $this;
    }

    public function getPageSize(): string
    {
        return $this->pageSize;
    }

    public function setPageSize(string $pageSize): self
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    private function checkDefaults($options = null): ?array
    {
        if (null === $options) {
            $options = [];
        }

        if (!\array_key_exists('binary', $options)) {
            $options['binary'] = \dirname(__DIR__) . '/../../../bin/wkhtmltopdf-amd64';
        }

        if (!\array_key_exists('path', $options)) {
            $options['path'] = \sys_get_temp_dir();
        }

        return $options;
    }
}
