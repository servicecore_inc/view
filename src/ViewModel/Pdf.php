<?php

namespace ServiceCore\View\ViewModel;

use Laminas\ApiTools\Hal\Collection;
use Laminas\ApiTools\Hal\Entity;
use Laminas\Paginator\Paginator;
use Laminas\View\Model\ViewModel;
use ServiceCore\View\Data\Provider;

class Pdf extends ViewModel
{
    private ?string $fileName = null;

    public function __construct(?array $variables = null, ?array $options = null)
    {
        parent::__construct($variables, $options);

        $this->terminate = true;
        $headers         = \array_change_key_case(\getallheaders());
        $params          = [
            'timeout' => 600,
            'tz'      => $headers['x-tz'] ?? null
        ];

        $this->setVariables([
            'params'   => \array_merge($this->getVariable('params', []), $params),
            'entities' => function () {
                return $this->getEntities();
            }
        ]);
    }

    public function setTemplate($template): self
    {
        return $this;
    }

    public function getTemplate(): string
    {
        return self::class;
    }

    public function getEntities()
    {
        $payload = $this->getPayload();

        if ($payload instanceof Entity) {
            $entity = $payload->getEntity();

            if ($entity instanceof Provider) {
                $this->fileName = $entity->getPdfFilename();
            }

            return [$entity];
        }

        if ($payload instanceof Collection) {
            $collection = $payload->getCollection();

            if ($collection instanceof Paginator) {
                $collection->setItemCountPerPage();
            }

            if ($collection instanceof Provider) {
                $this->fileName = $collection->getPdfFilename();
            }

            return $collection;
        }

        throw new \RuntimeException('Not supported');
    }

    public function getFilename(): ?string
    {
        return $this->fileName;
    }

    public function setPayload($payload): self
    {
        $this->setVariable('payload', $payload);

        return $this;
    }

    public function getPayload()
    {
        return $this->getVariable('payload');
    }

    /**
     * Override setTerminal()
     *
     * Does nothing; does not allow re-setting "terminate" flag.
     */
    public function setTerminal($flag = true): self
    {
        return $this;
    }
}
