<?php

namespace ServiceCore\View;

use Laminas\ModuleManager\Feature\ConfigProviderInterface;
use Laminas\Mvc\MvcEvent;
use Laminas\ServiceManager\ServiceManager;
use ServiceCore\View\RenderStrategy\Pdf as PdfRenderStrategy;

class Module implements ConfigProviderInterface
{
    public function getConfig(): array
    {
        return include \dirname(__DIR__) . '/config/module.config.php';
    }

    public function onBootstrap(MvcEvent $event): void
    {
        /** @var ServiceManager $serviceManager */
        $serviceManager = $event->getApplication()->getServiceManager();

        /** @var PdfRenderStrategy $pdfStrategy */
        $pdfStrategy = $serviceManager->get(PdfRenderStrategy::class);

        $pdfStrategy->attachShared($event->getApplication()->getEventManager()->getSharedManager());
    }
}
