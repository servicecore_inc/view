<?php

namespace ServiceCore\View\Plugin;

use Interop\Container\ContainerInterface;
use Laminas\Mvc\Controller\Plugin\AcceptableViewModelSelector as AcceptableViewModelSelectorParent;

/**
 * This overrides how the `getViewModel` method works slightly by checking the ServiceManager to see if the view model
 * is registered with it. This let's us use factories to build out custom view models that might need certain things
 * injected (ie: the entity manager for the Aging-Ar PDF renderer)
 */
class AcceptableViewModelSelector extends AcceptableViewModelSelectorParent
{
    /** @var ContainerInterface */
    private $services;

    public function __construct(ContainerInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    /**
     * @param array|null $matchAgainst
     * @param bool       $returnDefault
     * @param null       $resultReference
     *
     * @return mixed|\Laminas\View\Model\ModelInterface|null
     */
    public function getViewModel(
        ?array $matchAgainst = null,
        $returnDefault = true,
        &$resultReference = null
    ) {
        $name = $this->getViewModelName($matchAgainst, $returnDefault, $resultReference);

        // If wee have that view model in the service manager, fetch it from there. Otherwise use the parent's method
        // (was a simple `new $name()` at the time of this writing - doesn't allow dependency injection!)
        if ($this->services->has($name)) {
            return $this->services->get($name);
        }

        return parent::getViewModel($matchAgainst, $returnDefault, $resultReference);
    }
}
