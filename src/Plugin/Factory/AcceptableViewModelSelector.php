<?php

namespace ServiceCore\View\Plugin\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\View\Plugin\AcceptableViewModelSelector as AcceptableViewModelSelectorPlugin;

class AcceptableViewModelSelector implements FactoryInterface
{
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        ?array $options = null
    ): AcceptableViewModelSelectorPlugin {
        return new AcceptableViewModelSelectorPlugin($container);
    }
}
