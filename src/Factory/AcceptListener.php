<?php

namespace ServiceCore\View\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ApiTools\ContentNegotiation\AcceptListener as ZfAcceptListener;
use Laminas\ApiTools\ContentNegotiation\ContentNegotiationOptions;
use Laminas\ApiTools\ContentNegotiation\Factory\AcceptListenerFactory;
use ServiceCore\View\Plugin\AcceptableViewModelSelector;

class AcceptListener extends AcceptListenerFactory
{
    public function __invoke(ContainerInterface $container): ZfAcceptListener
    {
        return new ZfAcceptListener(
            $container->get(AcceptableViewModelSelector::class),
            $container->get(ContentNegotiationOptions::class)->toArray()
        );
    }
}
