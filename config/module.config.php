<?php

use Laminas\ApiTools\ContentNegotiation\AcceptListener;
use ServiceCore\View\Config\Create as CreateConfig;
use ServiceCore\View\Config\Factory\Create as CreateConfigFactory;
use ServiceCore\View\Factory\AcceptListener as AcceptListenerFactory;
use ServiceCore\View\Plugin\AcceptableViewModelSelector as ViewModelPlugin;
use ServiceCore\View\Plugin\Factory\AcceptableViewModelSelector as ViewModelPluginFactory;
use ServiceCore\View\Renderer\Factory\Pdf as PdfRendererStrategy;
use ServiceCore\View\Renderer\Pdf as PdfRenderer;
use ServiceCore\View\RenderStrategy\Factory\Pdf as PdfRenderStrategyFactory;
use ServiceCore\View\RenderStrategy\Pdf as PdfRenderStrategy;
use ServiceCore\View\ViewModel\Pdf as PdfViewModel;

return [
    'service_manager' => [
        'factories' => [
            ViewModelPlugin::class   => ViewModelPluginFactory::class,
            AcceptListener::class    => AcceptListenerFactory::class,
            PdfRenderStrategy::class => PdfRenderStrategyFactory::class,
            PdfRenderer::class       => PdfRendererStrategy::class,
            CreateConfig::class      => CreateConfigFactory::class,
        ]
    ],
    'view_manager'    => [
        'template_map' => [
            PdfViewModel::class => __DIR__ . '/../view/layout.phtml'
        ]
    ]
];
