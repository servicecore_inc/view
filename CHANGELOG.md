# CHANGELOG

All notable changes to this project will be documented in this file, in reverse chronological order by release.
This project does follow [Semantic Versioning](semver.org).

## 1.3.0
### Features
- Add the ability to import local files when using wkhtmltopdf
    - This allows the ability to import local fonts using the css command `url('file://...')`

## 1.2.3
### Fixed
- getallheaders() returns headers in PascalCase - this ensures 'x-tz' can be read correctly